import AdminAppointment from "../views/admin/Appointment";
import AdminAppointmentDetail from '../views/admin/AppointmentDetail'
import AdminLogin from "../views/AdminLogin";
import ClientResetPassword from "../views/ClientResetPassword";
import EmptyLayout from "../layouts/EmptyLayout";
import AdminDashboard from "../views/AdminDashboard";
import Services from "../views/admin/Services";
import NewsList from '../views/admin/news/news-list'
import NewsEdit from '../views/admin/news/news-edit'

const routes_admin = [
  {
    path: '/',
    meta: {layout: "admin"},
    name: 'AdminDashBoard',
    component: AdminDashboard
  },
  {
    path: '/login',
    meta: {layout: "login"},
    name: 'AdminLogin',
    component: AdminLogin
  },
  {
    path: '/forget-password',
    meta: {layout: "login"},
    name: 'ClientResetPassword',
    component: ClientResetPassword
  },
  {
    path: '/appointment',
    meta: {layout: "admin"},
    name: 'AdminAppointment',
    component: AdminAppointment
  },
  {
    path: '/appointments/:id',
    meta: {layout: "admin"},
    name: 'AdminAppointmentDetail',
    component: AdminAppointmentDetail
  },
  {
    path: '/services',
    meta: {layout: "admin"},
    name: 'Services',
    component: Services
  },
  {
    path: '/news',
    meta: {layout: "admin"},
    name: 'NewsList',
    component: NewsList
  },
  {
    path: '/news/:id/edit',
    meta: {layout: "admin"},
    name: 'NewsEdit',
    component: NewsEdit
  },
  {
    path: '*',
    name: 'Empty',
    component: EmptyLayout
  },
]

export default routes_admin
