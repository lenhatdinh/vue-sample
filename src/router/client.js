import EmptyLayout from "../layouts/EmptyLayout";
import ClientLogin from "../views/ClientLogin";
import ClientDashboard from '../views/clients/Dashboard';

const routes_client = [
  {
    path: '/',
    name: 'ClientDashboard',
    meta: {layout: "client"},
    component: ClientDashboard
  },
  {
    path: '/login',
    meta: {layout: "login"},
    name: 'UserLogin',
    component: ClientLogin
  },
  {
    path: '*',
    name: 'Empty',
    meta: {layout: "empty"},
    component: EmptyLayout
  },
]

export default routes_client