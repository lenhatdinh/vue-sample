const app = {
  namespaced: true,
  state: {
    alert_message: null,
    posts: [
      {
        id: 1,
        title: "abcdddd",
        publish_date: new Date("2020-07-14"),
        image: null,
        content: "",
        tags: "",
        source: "",
        categories: null,
        status: 'draft'
      }
    ]
  },
  mutations: {
    setAlertMessage(state, {message}) {
      state.alert_message = message
    },
    SET_POSTS(state, value){
      state.posts = value
    }
  },
  actions: {
    showMessage(context, {message}) {
      // Set global message.
      context.commit('setAlertMessage', {message})
      // Show alert.
      window.jQuery('#toast__app').toast('show')
    },
    showModal(context, {message}) {
      context.commit('setAlertMessage', {message})
      window.jQuery('#modal__app').modal('show')
    },
    setPosts({commit}, value){
      commit('SET_POSTS', value)
    },
    updatePosts({commit, state}, post){
      const foundIndex = state.posts.findIndex(item => item.id === post.id)
      console.log(foundIndex)
      if(foundIndex > -1){
        state.posts[foundIndex] = post
        console.log(state.posts);
      }

    }

  },
}

export default app