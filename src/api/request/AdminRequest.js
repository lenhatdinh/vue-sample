import BaseRequest from "../BaseRequest";

export default class AdminRequest extends BaseRequest {

  getAppointments(params) {
    return this.get('admin/appointments', params)
  }

  getAppointmentDetail(id) {
    return this.get('admin/appointments/' + id)
  }

  postAppointmentNotify(id) {
    return this.post('admin/appointments/' + id + '/notify')
  }

  postAppointmentNotifyAll(params) {
    return this.post('admin/appointment/notify-all', params)
  }

  postAppointmentNote(id, params) {
    return this.post('admin/appointments/' + id + '/note', params)
  }

  getAppointmentDocuments(id) {
    return this.get('admin/appointments/' + id + '/documents')
  }

  postAppointmentUploadDocument(id, params) {
    return this.post('admin/appointments/' + id + '/upload-document', params, {
      'Content-Type': 'multipart/form-data'
    })
  }

  postAppointmentRemoveDocument(id) {
    return this.post('admin/appointment-documents/' + id + '/remove')
  }

  postAppointmentInterpreter(id, params) {
    return this.post('admin/appointments/' + id + '/interpreter', params)
  }

  getAppointmentInterpreters(id) {
    return this.get('admin/interpreters')
  }

  getClinicApplicants() {
    return this.get('admin/clinic-applicants')
  }

  getClinicApproved() {
    return this.get('admin/clinic-approved')
  }

  getClinic(id) {
    return this.get('admin/clinics/' + id )
  }

  postClinicApprove(id) {
    return this.post('admin/' + id + '/approve')
  }

  postClinicDecline(id) {
    return this.post('admin/' + id + '/decline')
  }

}