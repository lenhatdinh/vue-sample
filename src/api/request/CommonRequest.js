import BaseRequest from "../BaseRequest";

export default class CommonRequest extends BaseRequest {
  getCountries() {
    return this.get('list-country');
  }

  getAppointmentMetas() {
    return this.get('appointment/metas')
  }

  getSettingNotification() {
    return this.get('auth/setting-notification')
  }

  updateSettingNotifications(params) {
    return this.post('auth/setting-notification', params)
  }
}