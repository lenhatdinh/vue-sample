export default class BaseRequest {
  getUrlPrefix() {
    return process.env.VUE_APP_BASE_URL + 'api/';
  }

  post(url, data = {}, headers = null) {
    return new Promise((resolve, reject) => {
      let options = {}
      if (headers) {
        options.headers = headers
      }

      window.axios
        .post(this.getUrlPrefix() + url, data, options)
        .then((response) => {
          if (response.data.status === 200) {
            resolve(response.data);
            return;
          }
          if (response.status === 200) {
            resolve(response)
            return
          }
          if (response.data.status === 406) {
            // window.location.href = "/login";
            return;
          }
          if (response.status === 501) {
            window.$store.dispatch('app/showModal', {message: response.message})
            return
          }
        })
        .catch((error) => {
          this._errorHandler(reject, error);
        });
    });
  }

  get(url, params) {
    return new Promise((resolve, reject) => {
      window.axios
        .get(this.getUrlPrefix() + url, {params})
        .then((response) => {
          if (response.data.status === 200) {
            resolve(response.data);
            return;
          }
          if (response.status === 200) {
            resolve(response)
            return
          }
          if (response.status === 501) {
            window.$store.dispatch('app/showModal', {message: response.message})
            return
          }
          // if(response.data.status === 406){
          //     window.location.href = "/login";
          //     return;
          // }
        })
        .catch((error) => {
          this._errorHandler(reject, error);
        });
    })
  }

  _errorHandler(reject, err) {
    return reject(err);
  }

}