module.exports = {
    gg:function(){
      return 3;
    },

    padStart(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    },
    getLocalAdmin() {
        return localStorage.getItem("admin") ? JSON.parse(localStorage.getItem("admin")) : null;
    },
    setLocalAdmin(user) {
        localStorage.setItem("admin", JSON.stringify(user));
    },
    removeLocalAdmin() {
        localStorage.removeItem("admin");
    },
    getLocalUser() {
        return localStorage.getItem("user") ? JSON.parse(localStorage.getItem("user")) : null;
    },
    setLocalUser(user) {
        localStorage.setItem("user", JSON.stringify(user));
    },
    removeLocalUser() {
        localStorage.removeItem("user");
    },
    getMessageFromCode(code) {
        switch (code) {
            case 204: {
                return " the request was successful but there is no representation to return";
            }
            default : {
                return null;
            }
        }

    },
    getToken() {
        return localStorage.getItem("token");
    }
}