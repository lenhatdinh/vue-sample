export class appHelper {
  /**
   * @returns {boolean}
   */
  static isProvider() {
    return process.env.VUE_APP_ENV == 'provider'
  }

  /**
   * @returns {boolean}
   */
  static isClient() {
    return process.env.VUE_APP_ENV == 'client'
  }

  /**
   * @returns {boolean}
   */
  static isAdmin() {
    return process.env.VUE_APP_ENV == 'admin'
  }
}