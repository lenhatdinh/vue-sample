export default {
  STATUS: {
    intToString(value) {
      switch (value) {
        case  0: {
          return "Pending";
        }
        case  1: {
          return "Confirmed";
        }
        case  2: {
          return "Canceled";
        }
        case  3: {
          return "Completed";
        }
        case  4: {
          return "Rescheduled";
        }
        default: {
          return "Unknown";
        }
      }
    },
    verifyToString(value) {
      switch (value) {
        case  0: {
          return "Pending";
        }
        case  1: {
          return "Confirmed";
        }
        case  2: {
          return "Declined";
        }
        default: {
          return "Unknown";
        }
      }
    },
    isPending(value) {
      return value === 0
    },
    isConfirmed(value) {
      return value === 1
    },
    isSchedule(value) {
      return value === 4
    },
    stringToInt(value) {
      switch (value.toLowerCase()) {
        case "pending": {
          return 0
        }
        case "confirmed": {
          return 1
        }
        case "canceled": {
          return 2
        }
        case "completed": {
          return 3
        }
        case 'rescheduled': {
          return 4;
        }
        default: {
          return -1
        }
      }
    },
    intToExtraString(value) {
      switch (value) {
        case 1: {
          return 'fa fa-check'
        }
        case 0:
        case 2:
        case 4: {
          return 'fa fa-clock-o'
        }
        case 3: {
          return 'fa fa-minus'
        }
      }
    },
    intToExtraClass(value) {
      switch (value) {
        case 1: {
          return 'text-success'
        }
        case 0:
        case 2:
        case 4: {
          return 'text-danger';
        }
        case 3: {
          return ''
        }
      }
    },
    intToVerifyClass(value, prefix = 'alert') {
      switch (value) {
        case  0: {
          return prefix + "-warning";
        }
        case  1: {
          return prefix + "-success";
        }
        case  2: {
          return prefix + "-muted";
        }
        default: {
          return "";
        }
      }
    },
    intToStatusClass(value, prefix = 'alert') {
      switch (value) {
        case  0: {
          return prefix + "-warning";
        }
        case  1: {
          return prefix + "-success";
        }
        case  2: {
          return prefix + "-danger";
        }
        case  3: {
          return prefix + "-primary";
        }
        case  4: {
          return prefix + "-info";
        }
        default: {
          return "";
        }
      }
    }
  },
  APPOINTMENT: {
    MODE: {
      intToString(value) {
        switch (value) {
          case 1:
            return "audio/video";
          case 2:
            return "site visit";
          case 3:
            return "so";
          default:
            return "unknown"
        }
      }
    }
  }
}